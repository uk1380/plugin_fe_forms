<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * 
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

delete_option('gatid');
delete_option('gatids');
delete_option('fbid');
delete_option('fbids');
delete_option('fe_form');

