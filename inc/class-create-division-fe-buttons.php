<?php
/**
 * 
 * 
 * 
 */


 class createDivisionFEButtons
 {

    public function __construct() {


		$this->load_dependencies();

	}
    
    public function load_dependencies(){
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'inc/admin/FEButtonsAdminSettings.php';

        $FEButtonsAdminSettings = new FEButtonsAdminSettings();
        return $FEButtonsAdminSettings->register();

     }

    public function generate_fe_buttons_shortcode(){
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'inc/public/generate-fe-buttons-html.php';
        $generateFeButtons = new GenerateJimsFeButtonsHtml();

        return $generateFeButtons->generateFeButtons();

        
     }

    function generate_fe_form_shortcode(){
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'inc/public/generate-fe-form-html.php';
        $generateFeForm = new GenerateJimsFeFormHtml();

        return $generateFeForm->generateFeForm();

        
    }

 }
