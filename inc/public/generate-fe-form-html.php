<?php
/**
 * generate the fe form html 
 * 
 * 
 */

 class GenerateJimsFeFormHtml
 {
     public function generateFeForm(){

        extract(shortcode_atts(array(
            'divisionid' => isset($_GET['divisionid']) ? $_GET['divisionid'] : '',
            'gatid' => isset($_GET['gatid']) ? $_GET['gatid'] : '',
            'fbid' => isset($_GET['fbid']) ? $_GET['fbid'] : ''
        ), $atts));
        ?>

        <!-- <style>.resp-iframe-container{position:relative;overflow:hidden}.resp-iframe-container > iframe{position:absolute;top:0;left:0;width:100%;height:100%;border:0}</style> -->
        <div class="resp-iframe-container">
        <?php
        return sprintf("<iframe class='fe_form' src='https://fe.jims.net/division/%s/?GATID=%s&FBID=%s'></iframe>",$divisionid,$gatid,$fbid);
     }
 }