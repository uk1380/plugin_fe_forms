<?php
/***
 * 
 */
class FEButtonsAdminSettings
{

	public function __construct(){
		// Hook into the admin menu
		add_action( "admin_menu", array($this, 'create_plugin_settings_page') );

		add_action( 'admin_init', array( $this, 'setup_sections' ) );
		
		add_action( 'admin_init', array( $this, 'setup_fields' ) );		


	}

	public function register(){

	}


	public function create_plugin_settings_page(){
		// Add the menu item and page
		$page_title = 'Jims FE buttons Settings Page';
		$menu_title = 'Jims FE buttons';
		$capability = 'manage_options';
		$slug = 'button_id_setup';//setting page name and also is setting group name
		$callback = array( $this, 'plugin_settings_page_content' );
		$icon = 'dashicons-admin-plugins';
		$position = 100;
	
		add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
	}
	

	public function plugin_settings_page_content() { 
		?>
		<div class="wrap">
			<h2>Jims FE Form Setting Page</h2>
			<p>This plugin is to create the Franchisee form buttons<p>
			<p>Please use shortcode [fe_buttons] to create the FE buttons page, [fe_forms] to create the FE form page.</p>
			<p>
			<form method="post" action="options.php">
				<?php
					settings_fields( 'button_id_setup' );//setting group name
					do_settings_sections( 'button_id_setup' );//setting group name
					submit_button();
				?>
			</form>
		</div> 
		<?php
	}

	public function setup_sections() {
		// add_settings_section( $id, $title, $callback, $page );
		add_settings_section( 'gatid_section', 'Goolge Analystic ID Input Area', array( $this, 'section_callback' ), 'button_id_setup' );
		add_settings_section( 'our_second_section', 'FaceBook ID Input Area', array( $this, 'section_callback' ), 'button_id_setup' );
	}

	public function section_callback( $arguments ) {
		switch( $arguments['id'] ){
			case 'gatid_section':
				echo 'This is the first description here!';
				break;
			case 'our_second_section':
				echo 'This one is number two';
				break;
		}	
	}

	public function setup_fields() {
		$fields = array(
			array(
				'uid' => 'gatid',
				'label' => 'GATID:',
				'section' => 'gatid_section',
				'type' => 'text',
				'options' => false,
				'placeholder' => 'UA-xxxxxxxxx-x',
				'helper' => "eg. UA-xxxxxxxxx-x",
				'supplemental' => "It's the default GATID for all domains, if a domain hasn't an override ID, this should be the GATID value.",
				'default' => ''
			),
			array(
				'uid' => 'gatids',
				'label' => 'GATIDS:',
				'section' => 'gatid_section',
				'type' => 'textarea',
				'options' => false,
				'placeholder' => '',
				'helper' => "",
				'supplemental' => "GATIDs textarea example input format:
					jims.net UA-xxx-xxx-xx, jimshandyman.net UA-xxx-xxxx-xx, jimsshadesails.com.au UA-xxx-xxxx-xx",
				'default' => ''
			),
			array(
				'uid' => 'fbid',
				'label' => 'FBID:',
				'section' => 'our_second_section',
				'type' => 'text',
				'options' => false,
				'placeholder' => 'xxxxxxxxxxxx',
				'helper' => "eg. xxxxxxxxxxxx",
				'supplemental' => "It's the default FBID for all domains, if a domain hasn't an override ID, this should be the FBID value.",
				'default' => ''
			),
			array(
				'uid' => 'fbids',
				'label' => 'FBIDS:',
				'section' => 'our_second_section',
				'type' => 'textarea',
				'options' => false,
				'placeholder' => '',
				'helper' => "",
				'supplemental' => "FBIDs textarea example input format: jims.net 21456498789797, jimshandyman.net 6565565653243232",
				'default' => ''
			),
			array(
				'uid' => 'fe_form',
				'label' => 'Choose page to display the fe form:',
				'section' => 'our_second_section',
				'type' => 'select',
				'options' => true,
				'placeholder' => 'DD/MM/YYYY',
				'helper' => '',
				'supplemental' => 'Choose the page to add shortcode [fe_forms]',
				'default' => ''
			)
		);

		foreach( $fields as $field ){
			// add_settings_field( $id, $title, $callback, $page, $section, $args );
			add_settings_field( $field['uid'], $field['label'], array( $this, 'field_callback' ), 'button_id_setup', $field['section'], $field );
			
			register_setting( 'button_id_setup', $field['uid'] );
		}
	}

	public function field_callback( $arguments ) {
		$value = get_option( $arguments['uid'] ); // Get the current value, if there is one
		
		if( ! $value ) { // If no value exists
        	$value = $arguments['default']; // Set to our default
    	}

    	// Check which type of field we want
    	switch( $arguments['type'] ){
        	case 'text': // If it is a text field
            	printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
			break;
			case 'textarea': // If it is a textarea
        		printf( '<textarea name="%1$s" id="%1$s" placeholder="%2$s" rows="5" cols="50">%3$s</textarea>', $arguments['uid'], $arguments['placeholder'], $value );
			break;
			case 'select': // If it is a select dropdown
			printf( wp_dropdown_pages( array( 'name' => 'fe_form', 'echo' => 0, 'show_option_none' => __( '&mdash; Select &mdash;' ), 'option_none_value' => '0', 'selected' => get_option( 'fe_form' ) ) ));
			
			printf('the Chosen page is: '. get_permalink(get_option( 'fe_form' )));
			break;
    	}

		// If there is help text
		if( $helper = $arguments['helper'] ){
			printf( '<span class="helper"> %s</span>', $helper ); // Show it
		}

		// If there is supplemental text
		if( $supplimental = $arguments['supplemental'] ){
			printf( '<p class="description">%s</p>', $supplimental ); // Show it
		}

	}
}