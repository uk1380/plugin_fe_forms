<?php
/*
Plugin Name: Create Jims Divisions Franchise Enquires Form
Plugin URI: https://jims.net
Description: Displays a grid or list with Jim's Divisions Request A Quote Buttons
Version: 1.0
Author: Georgios Antoniou, Jett
Author URI: https://plegma.host
License: GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.txt
*/
 
/**
 * Adding Submenu under Settings Tab
 *
 * @since 1.0
 */
if (! defined('ABSPATH')) {
	die;
}


/**
 * Include CSS file for fe button plugin.
 */
function setting_up_scripts() {
	wp_register_style( 'create-division-fe-buttons', plugin_dir_url( __FILE__ ) . 'assets/style.css');

    wp_enqueue_style( 'create-division-fe-buttons' );

}
add_action('wp_enqueue_scripts','setting_up_scripts');


/**
 * create fe button and fe form shortcodes.
 */
require_once plugin_dir_path( __FILE__ ) . 'inc/class-create-division-fe-buttons.php';

if( class_exists( 'createDivisionFEButtons' ) ) {
	$createDivisionFEButtons = new createDivisionFEButtons();
}

add_shortcode( 'fe_buttons', array('createDivisionFEButtons','generate_fe_buttons_shortcode'));

add_shortcode( 'fe_forms', array('createDivisionFEButtons','generate_fe_form_shortcode'));



//activation
// register_activation_hook(__FILE__, array($createDivisionFeButtons,'activate'));

// //deactivation
// register_deactivation_hook(__FILE__, array($createDivisionFeButtons,'deactivate'));

//unistall
// register_deactivation_hook(__FILE__, array($createDivisionFeButtons,'unistall'));








